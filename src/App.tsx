import AuthorizationForm from './Authorization/LoginForm';

function App() {
  function OnAuthorize(): void{
    fetch('todos/' + Math.floor(Math.random() * (10 - 1 + 1)) + 1)
    .then(response => response.text().then(function(text){
      alert(text);
    }));
  }  

  return (
    <AuthorizationForm authorizationHandler={OnAuthorize}/>
  );
}

export default App;
