import React, {Component} from 'react';
import './LoginForm.css';
import './TdLabeledInput';
import TdLabeledInput from './TdLabeledInput';

export interface IAuthorizationFormProps{
    authorizationHandler: ()=>void;
}

export default class AuthorizationForm extends Component<IAuthorizationFormProps>{  
    onAuthorizeClick = () => {
        this.props.authorizationHandler();
    }
    
    render(){
        return(
            <div>
                <center>                    
                    <h1 className='LoginForm-h1'>Необходимо авторизоваться</h1>
                    <br/>
                    <table className='LoginForm-table'>
                        <tr className='LoginForm-tr-special'></tr>
                        <tr>
                            <TdLabeledInput placeholderText='введите логин' labelText='Логин:'/>
                            <td className='LoginForm-td-special'></td>
                        </tr>
                        <tr>
                            <TdLabeledInput placeholderText='введите пароль' labelText='Пароль:'/>                            
                            <td className='LoginForm-td-special'></td>
                        </tr>
                        <tr>
                            <td></td>
                            <td>
                                <button onClick={this.onAuthorizeClick}>Авторизоваться</button>
                            </td>
                            <td className='LoginForm-td-special'></td>
                        </tr>
                        <tr className='LoginForm-tr-special'></tr>
                    </table>
                </center>
            </div>
        );
    }
}