export interface ITdLabeledInput {
    labelText: string,
    placeholderText: string
}

export default function TdLabeledInput(props: ITdLabeledInput) {
    const { labelText, placeholderText } = props;

    return (
        <>
            <td>
                <label>{labelText}</label>
            </td>
            <td>
                <input placeholder={placeholderText}></input>
            </td>
        </>
    );

}